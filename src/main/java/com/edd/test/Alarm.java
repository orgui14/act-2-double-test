package com.edd.test;

public class Alarm
{
    private final double LowPressureThreshold = 17; //Umbral de baja presión
    private final double HighPressureThreshold = 21; //HighPressureThreshold

    private Sensor sensor;

    public Alarm(Sensor sensor){
        this.sensor = sensor;
    }

    private boolean alarmOn = false;

    public void check()
    {
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }


    public void checkParaModificarCambiosPresion(double psiPressureValue)
    {

        if (psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue) {
            alarmOn = true;
        }
    }

    public boolean isAlarmOn()
    {

        return alarmOn;
    }
}

/**
 1. Refactorizar el código de la clase Alarma para que sea testable (Utilizando DI inyección de dependencias).

 2. Completar los cuerpos de los test indicados siguiente las instrucciones.
 Haz 2 versiones,
 una utilizando Mockito API, y otro sin utilizar Mockito API.

 Alarm Class: Monitoriza la presión de los neumáticos y establece una alarma si la presion cae
 por encima o por debajo del rango esperado.
 Sensor Class: Simula el comportamiento de un sensor de presión real proporcionando
 presiones reales però aleatorias.
 */