package com.edd.test;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class AlarmMockitoTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    public void testAlarmIsNotOnByDefault() {
        Alarm alarm = mock(Alarm.class);
        assertFalse(alarm.isAlarmOn());
    }
    /**
     * Comprobar que la alarma se activa en con bajas presiones
     */
    @Test
    public void testAlarmOnWithLowPressure() {
        Sensor sensor = mock(Sensor.class);
        when(sensor.popNextPressurePsiValue()).thenReturn(16.0);
        Alarm alarm = new Alarm(sensor);
        alarm.check();
        assertTrue(alarm.isAlarmOn());
    }

    /**
     * Comprobar que la alarma se activa en con altas presiones
     */
    @Test
    public void testAlarmOnWithHighPressure() {
        Sensor sensor = mock(Sensor.class);
        when(sensor.popNextPressurePsiValue()).thenReturn(22.0);
        Alarm alarm = new Alarm(sensor);
        alarm.check();
        assertTrue(alarm.isAlarmOn());


    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones normales
     */
    @Test
    public void testAlarmOffWithNormalPressure() {
        Sensor sensor = mock(Sensor.class);
        when(sensor.popNextPressurePsiValue()).thenReturn(18.0);
        Alarm alarm = new Alarm(sensor);
        alarm.check();
        assertFalse(alarm.isAlarmOn());

        when(sensor.popNextPressurePsiValue()).thenReturn(18.5);
        alarm = new Alarm(sensor);
        alarm.check();
        assertFalse(alarm.isAlarmOn());

        when(sensor.popNextPressurePsiValue()).thenReturn(19.3);
        alarm = new Alarm(sensor);
        alarm.check();
        assertFalse(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones límite
     */
    @Test
    public void testAlarmOffWithLimitsPressure() {

        Sensor sensor = mock(Sensor.class);
        when(sensor.popNextPressurePsiValue()).thenReturn(17.0);
        Alarm alarm = new Alarm(sensor);
        alarm.check();
        assertFalse(alarm.isAlarmOn());

        when(sensor.popNextPressurePsiValue()).thenReturn(21.0);
        alarm = new Alarm(sensor);
        alarm.check();
        assertFalse(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @Test
    public void testAlarmOnWithLimitsPressure() {
        Sensor sensor = mock(Sensor.class);
        when(sensor.popNextPressurePsiValue()).thenReturn(16.9);
        Alarm alarm = new Alarm(sensor);
        alarm.check();
        assertTrue(alarm.isAlarmOn());

        when(sensor.popNextPressurePsiValue()).thenReturn(21.1);
        alarm = new Alarm(sensor);
        alarm.check();
        assertTrue(alarm.isAlarmOn());

    }
    
}
