package com.edd.test;


import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.Assert.*;


public class AlarmTest {

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    public void testAlarmIsNotOnByDefault() {
        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);
        assertFalse(alarm.isAlarmOn());
    }
    /**
     * Comprobar que la alarma se activa en con bajas presiones
     */
    @Test
    public void testAlarmOnWithLowPressure() {
      //No es posible simular la baja presion sin modificar el codigo de Alarm modificando check()
        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);
        alarm.checkParaModificarCambiosPresion(16.0);
        assertTrue(alarm.isAlarmOn());
    }

    /**
     * Comprobar que la alarma se activa en con altas presiones
     */
    @Test
    public void testAlarmOnWithHighPressure() {
        //No es posible simular la alta presion sin modificar el codigo de Alarm modificando check()
        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);
        alarm.checkParaModificarCambiosPresion(22);
        assertTrue(alarm.isAlarmOn());


    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones normales
     */

    @Test
    public void testAlarmOffWithNormalPressure() {

        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);
        alarm.checkParaModificarCambiosPresion(18.0);
        assertFalse(alarm.isAlarmOn());

        alarm.checkParaModificarCambiosPresion(18.5);
        assertFalse(alarm.isAlarmOn());

        alarm.checkParaModificarCambiosPresion(19.3);
        assertFalse(alarm.isAlarmOn());



    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones límite
     */
    @Test
    public void testAlarmOffWithLimitsPressure() {

        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);
        alarm.checkParaModificarCambiosPresion(17.0);
        assertFalse(alarm.isAlarmOn());

        alarm.checkParaModificarCambiosPresion(21.0);
        assertFalse(alarm.isAlarmOn());

    }

    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @Test
    public void testAlarmOnWithLimitsPressure() {

        Sensor sensor = new Sensor();
        Alarm alarm = new Alarm(sensor);
        alarm.checkParaModificarCambiosPresion(16.9);
        assertTrue(alarm.isAlarmOn());

        alarm.checkParaModificarCambiosPresion(21.1);
        assertTrue(alarm.isAlarmOn());

    }
    
}
